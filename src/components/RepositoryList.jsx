import {useState, useEffect} from "react"
import { Counter } from "./Counter"
import { RepositoryItem } from "./ReposositoryItem"
import '../styles/repositories.scss'
import react from "react"
// https://api.github.com/orgs/rocketseat/repos
// passando uma variável como propriedade
const repositoryName = "unform2"
// Passando um array de objeto como propriedades
const repository = {
    name: 'unform',
    description: 'Forms in React',
    link: 'https://github.com/'
}



export function RepositoryList(){
    const [repositories, setRepositories] = useState([])
    useEffect(()=> {
        fetch('https://api.github.com/orgs/rocketseat/repos')
        .then(response => response.json())
        .then(data => setRepositories(data)) //Aqui poderia fazer diversãso coisas com esses dados
    }, []);

    // perceba que toda vez que o estado e alterado ele é renderizado noavamente.
    console.log(repositories);

    return(
        <section className="repository-list">
            <h1>Repository List</h1>
           
            <ul>
                  {/* Informando que vou trabalhar com o código javascript */}
                  {
                    // Utiliando funções do javascript para renderizar os valores nos componentes
                    repositories.map(repository => {
                        return <RepositoryItem key={repository.name} repository={repository}/>
                    }) 
                }
            </ul>
            <h1>Trabalando com Estados</h1>
            <Counter/>

        </section>
    )
}